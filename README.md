# Web Filter
Welcome to Web Filter!  This project updates the /etc/hosts file in your system to block ads.  In addition, it also blocks other distractions like news sites and social networking sites.  (You are free to remove domains from the /etc/hosts file.  Just remember that only the root/admin user can edit this file.)
